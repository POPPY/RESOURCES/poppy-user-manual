# poppy-user-manual

POPPY User Manual source files
==============================

This directory contains the source files to generate the POPPy framework user manual with sphinx tool (https://www.sphinx-doc.org/en/master/).

The POPPy user manual is avaible in https://poppy-framework.readthedocs.io/en/latest/.