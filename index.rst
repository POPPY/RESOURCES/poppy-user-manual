.. POPPy framework documentation master file, created by
   sphinx-quickstart on Thu Jul  9 16:23:50 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

POPPy framework Documentation
=============================

.. toctree::
    :maxdepth: 2

    User Manual<sections/pipeline/index>
    Developper Guide<sections/developer_guide>
.. Building installations<installation/index>
.. Appendices<appendices/index>


.. remove this section once in the last version of the document

TODO list
=========

.. todolist::
