**********
Procedures
**********

This section describes some procedures that must be applied in some situations
when working with the pipeline.

New database installation
=========================

If a database has just been created, the structure of the ROC database must be
applied on it:

.. code-block:: bash

    $ pop roc migrate current set $PLUGIN

for the plugin :code:`$PLUGIN` and then upgrade it to the version needed for
this plugin.

.. code-block:: bash

    $ pop roc migrate upgrade $PLUGIN $REVISION

After a new database, the database must be filled with the informations from
the RPL informations, descriptors from plugins before any use.

If the IDB directory from MEB-GSE is present in :code:`$IDB` path:

.. code-block:: bash

    $ pop rpl load --idb $IDB/xml/RPW_IDB.xml \
        --mapping $IDB/xml/IDB_SRDB_Mapping_Table_by_MapGen.xml

then upload descriptors from plugins into the database.

.. code-block:: bash

    $ pop piper descriptor

New version of the IDB
======================

If a new version of the IDB must be integrated into the ROC pipeline:

1. load the IDB into the ROC database.

.. code-block:: bash

    $ pop rpl load pop rpl load --idb $IDB/xml/RPW_IDB.xml \
        --mapping $IDB/xml/IDB_SRDB_Mapping_Table_by_MapGen.xml

2. recreate excel skeletons files for the HK from the IDB files:

.. code-block:: bash

    $  pop film skeleton --idb $IDB/xml/RPW_IDB.xml \
        --mapping $IDB/xml/IDB_SRDB_Mapping_Table_by_MapGen.xml \
        --output-dir /output/path/for/excel/files/

3. regenerate CDF master files from skeletons and add them to the versioning of
   the ROC-SGSE.

3. update any scripts that needs the IDB version and that is hardcoded in it.
   For example, scripts for launching the TV-SGSE at CNES, or on roc-dev.

Documentation
=============

To change the template of PDF output of the documentation,
:file:`preambule.tex` can modify. For some parameters used in the title page,
such as the reference of the document, etc, :file:`parameters.sty` must be
updated. Both files are present inside the :file:`source` directory of the
documentation.

A makefile is present at the root of the directory of each document. Using:

.. code-block:: bash

    $ make latexpdf

for a PDF output with LaTex and:

.. code-block:: bash

    $ make html

for an HTML output.

All resulting files are present in the :file:`build` directory.

An API documentation can be generated with the :code:`sphinx-apidoc` command
provided by :code:`sphinx`. See its documentation for how to proceed.

