****************
Developper Guide
****************

The goal of this chapter is to provide information on how POPPy works in the
inside.
It can be useful to add features to the framework.


Workflow
========

.. _launch_workflow:

.. figure:: images/launch_workflow.*
    :width: 75%

    Workflow of the launching of a POPPy pipeline

The pipeline is launched through a :func:`poppy.pop.scripts.scripts.main`
function starting the generation of the commands with the dedicated command
module and then launches the good command according to the content of the CLI.
A representation of the process is given in :numref:`launch_workflow`.

For details on the process:

#. The :class:`poppy.core.command.CommandManager` starts generating the
   commands. It sends the
   :attr:`poppy.core.command.CommandManager.generation_start` signal to
   inform connected objects that a generation started. A setup function as been
   connected at the import of the module to the command manager and is called
   before the generation.
#. In the :func:`poppy.pop.scripts.scripts.setup` function, the available
   plugins are loaded from the :file:`settings.py` file where activated plugins
   are inserted. This file is in the pipeline root directory.
#. Since we introduce new code in the pipeline through plugins, we need to add
   our handlers of the logs into the loggers defined by the plugins
   (:func:`poppy.pop.scripts.scripts.setLogger`).
#. Plugins are loaded if we can import them correctly, and if the process of
   loading a plugin into the pipeline worked. See :ref:`plugins`.
#. All slots of the signal have been treated it, so the main function says to
   the :class:`rgts_library.tools.command.CommandManager` to launch the command
   from the informations provided in the CLI. A signal
   :attr:`rgts_library.tools.command.CommandManager.run_start` with the parsed
   CLI arguments in argument of the signal is emitted. An
   :func:`poppy.pop.scripts.scripts.pipeline_init`
   function has already been connected to this signal at module importation.
#. The init of the pipeline starts by loading the configuration file selected
   on the CLI or at the default path and read the data inside it. It checks
   that the configuration file is valid against the schema.
#. From information inside the configuration file, the databases are loaded
   (:func:`rgts_library.tools.database.load_databases`).
#. Finally, the descriptor is also loaded. With it, the pipeline can access all
   information on versions for softwares as needed.

Then the selected command takes the control and run.



API
---

.. automodule:: poppy.pop.scripts.scripts
    :members:
    :show-inheritance:

.. autofunction:: poppy.pop.scripts.scripts.loadPlugins
.. autofunction:: poppy.pop.scripts.scripts.pipeline_init
.. autofunction:: poppy.pop.scripts.scripts.setup
.. autofunction:: poppy.pop.scripts.scripts.setLogger



