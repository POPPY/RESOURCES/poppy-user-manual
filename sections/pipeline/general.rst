*******
General
*******

Scope of the document
=====================

This document is the POPPy framework user manual.
It describes how to install and use the framework.
It also provides tutorials for users.

Applicable documents
====================

.. list-table::

    * - Mark
      - Reference/Issue/Revision
      - Title of the document

Reference documents
===================

.. list-table::

    * - Mark
      - Reference/Issue/Revision
      - Title of the document


