Pipeline description
====================

.. toctree::
    General<general>
    Introduction<introduction>
    Migration<migration>
    Pipeline<pipeline>
    Tasks<tasks>
    Targets<targets>
    Commands<commands>
    Descriptor<descriptor>