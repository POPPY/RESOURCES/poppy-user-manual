**********
Descriptor
**********

Introduction
============

A pipeline built with the POPPy framework usually needs several plugins to
generate different data.
For traceability, the pipeline needs to track the versions of the
plugins, data format used to generate data products. This is done with the
registration of the plugins information through a *descriptor* file,
providing the interface and information necessary to the pipeline.

The following sections describe the *descriptor* interface and the process for
loading plugins using the descriptor file.

Descriptor interface
====================

There is two kind of descriptors:

* one for the pipeline,
* one for each plugins.

Pipeline descriptor
-------------------

The pipeline descriptor provides metadata associated to the pipeline, databases
and used plugins. All information is inside :code:`pipeline`.

Identification
^^^^^^^^^^^^^^

The identity of the pipeline is defined with the following JSON objects:

* :code:`identifier`: the identifier of the pipeline.
* :code:`name`: a human readable name for the pipeline.
* :code:`description`: the purpose of the pipeline.

Release
^^^^^^^

The :code:`release` object shall inform about the current S/W release. It shall
contain the following attributes:

* :code:`version`: current version of the pipeline in the format
  'MAJOR.MINOR.REVISION', following the ROC conventions [AD2].
* :code:`date`: date and hour of the release of the S/W in the format
  ‘YYYY-MM-DD’, where ‘YYYY’, ‘MM’ and ‘DD’ are respectively the year, month
  and date of the release.
* :code:`author`: name of the person, team or entity responsible of the
  release.
* :code:`contact`: contact of the author (e.g., email)
* :code:`institute`: name of the institute that delivers the release.
* :code:`modification`: a string containing the list of S/W modifications in
  the current release.

In addition, the :code:`release` object can provide the following optional
attributes:

* :code:`file`: file name to reference a data schema, master CDF, etc.
* :code:`reference`: name of a file as reference for the documentation, used as
  an indication for the ROC team.
* :code:`url`: indication for an online resource.

Project
^^^^^^^

The project field contains information used for the auto-generation of some CDF
skeleton files.

* :code:`name`: name of the project as to set in the skeleton.
* :code:`source`: the source of the data.
* :code:`provider`: the provider of the data.
* :code:`discipline`: category of the data.
* :code:`PI`: informations on the Principal Investigator.

  + :code:`PI.name` for the name of the PI.
  + :code:`affiliation` for where the PI is affected.
* :code:`instrument_type`: the instrument used in the project.
* :code:`mission_group`: the group of the mission.

Databases
^^^^^^^^^

The :code:`databases` object contains a list of object, representation of the
databases used by the pipeline and their metadata for future references. Each
database database follows this structure.

* :code:`identifier`: the identifier of the pipeline.
* :code:`name`: a human readable name for the pipeline.
* :code:`description`: the purpose of the pipeline.
* :code:`release`: a release object as defined for the :code:`release` of the
  pipeline. The structure must be the same.


Calibration softwares
^^^^^^^^^^^^^^^^^^^^^

:code:`calibration_softwares` contains the list of paths to the external
calibration softwares whose descriptor must be loaded into the ROC database.

.. _plugin_descriptor:

Plugin descriptor
-----------------

The descriptor is a file in the JSON format located inside the
:file:`namespace/plugin/descriptor.json`.

A descriptor file is validated against a schema located inside the
:file:`poppy/pop/config/plugin-descriptor-schema.json`.

Identification
^^^^^^^^^^^^^^

Each S/W will be identified in the pipeline by the attributes provided in the
:code:`identification` JSON object:

* :code:`project`: name of the project. It shall be "ROC-SGSE" for S/W used in
  the ROC-SGSE pipeline, and "RPW" otherwise.
* :code:`name`: a cool name for the calibration software, to be human readable.
* :code:`identifier`: a unique name used as reference by the ROC-SGSE pipeline
  to identify the S/W. It shall contain Latin alphabet uppercase letters only.
  For ease of use your name should be :code:`namespace.plugin`. In all case
  , the developer team shall validate the identifier (ID) to avoid duplicated
  names.
* :code:`description`: short description of the software. This description will
  be saved in the ROC database.

.. _plugin_release:

Release
^^^^^^^

The :code:`release` object shall inform about the current S/W release. It is
also used to describe the output data (see section :ref:`outputs`). It shall
contain the following attributes:

* :code:`version`: Current version of the S/W in the format
  'MAJOR.MINOR.REVISION', following the ROC conventions [AD2].
* :code:`date`: Date and hour of the release of the S/W in the format
  ‘YYYY-MM-DD’, where ‘YYYY’, ‘MM’ and ‘DD’ are respectively the year, month
  and date of the release.
* :code:`author`: Name of the person, team or entity responsible of the release
* :code:`contact`: contact of the author (e.g., email)
* :code:`institute`: Name of the institute that delivers the release
* :code:`modification`: a string containing the list of S/W modifications in
  the current release.

In addition, the :code:`release` object can provide the following optional
attributes:

* :code:`file`: file name to reference a data schema, master CDF, as an
  indication for the ROC team. Only required in the outputs modes object
  descriptions.
* :code:`reference`: name of a file as reference for the documentation, used as
  an indication for the ROC team.
* :code:`url`: indication for an online resource.

.. _tasks_descriptor:

Tasks
^^^^^

The :code:`tasks` object contains the list of tasks that the plugin defines.
For each task, its name, its purpose and the list of input/output datasets to
be read/saved shall be supplied. It allows the pipeline to control if the
expected output data files are correctly saved.

Each function listed in the :code:`tasks` object shall contain the following
attributes and JSON arrays:

* :code:`name`: the name of the task. It will be used as internal reference by
  the pipeline.
* :code:`description`: a short description of the purpose of the function.
* :code:`inputs`: a JSON object containing the list of the input datasets
  required by the task.
* :code:`outputs`: a JSON object containing the list of the outputs
  returned by the task.

The purpose and the content of the :code:`inputs` and :code:`outputs` are
detailed in the two next sections.

.. _inputs:

Inputs
^^^^^^

The pipeline requires information in order to identify the specific input
parameters of a given task. This is the aim of the :code:`inputs` object, which
provides the list of the specific input parameters in JSON objects, with the
two following mandatory attributes:

* :code:`identifier`: The dataset ID associated to the input data file, as
  referenced in the ROC system.
* :code:`version`: The version of the input data file. This allows the ROC
  pipeline to ensure that S/W will process the right version of the input file.

.. _outputs:

Outputs
^^^^^^^

The pipeline requires information in order to verify that the expected output
data files have been correctly produced at the end of a given task execution.

Each JSON object listed in code:`outputs` shall described in details the
corresponding dataset, using the following attributes/objects:

* :code:`identifier`: The dataset ID associated to the output, as referenced in
  the ROC system. It shall be unique and comply the naming convention listed in
  [AD2].
* :code:`name`: a more human-readable name for the dataset, not necessarily
  unique.
* :code:`description`: a short description of the dataset.
* :code:`level`: the processing level of the dataset. Allowed values are "LZ",
  "L0", "L1", "L2", "L2R", "L2S", "L3", "L4", "AUX", "LL0", "LL1", "HK".
* :code:`release`: information about the release of the dataset. The structure
  is the same as defined in the section :ref:`plugin_release`. In the case of a
  dataset using the CDF format, the "file" attribute shall provide the name of
  the master CDF file used to generate the output data files.

In any case, the POPPy framework will perform an automated validation of the
descriptor file at each new release, in order to check that the file content is
consistent with the ROC database information. In particular, it will verify
that the datasets declared the :code:`tasks` object are all defined in the
descriptor and across S/W.

Example
^^^^^^^

An example file taken from the :mod:`tuto.texter` module.

.. literalinclude:: ./plugin_descriptor.json
    :language: json
    :linenos:
