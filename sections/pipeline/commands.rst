********
Commands
********

New commands can be easily added to the pipeline through the command framework.

Creating a new command
======================

Commands are managed by the pipeline, and those the module containing the
framework for the pipeline is defined in :py:mod:`poppy.pop.command`. To create
a new command, simply derive a class from :py:class:`Command`.

Simple command
--------------

For example, we will create a command to display informations on the pipeline,
such as who wrote the pipeline.

.. code-block:: python

    from poppy.pop import Command

    class WhoCommand(Command):
        """
        Define a command displaying informations on the pipeline author.
        """
        # a name to reference the command
        __command__ = "who_command"

        # the name of the command used
        __command_name__ = "who"

        # the parent of the command
        __parent__ = "master"

        # the help message displayed to the user
        __help__ = "Show the author of the pipeline"

        def __call__(self, args):
            print("Super Mario, pipeline expert")

That's all!

Calling the pipeline with the *who* sub-command will display the name of the
author. Now the details of the parameters.

The :py:class:`WhoCommand` inherits the :py:class:`Command`. By doing that, the
:py:class:`WhoCommand` is automatically registered by the framework, and the
command is made available to the pipeline. Some attributes allows to perform
some selection and modify the behaviour of the command.

+
    :py:attr:`WhoCommand.__command__` is used to give a reference name to the
    command. If the command associated to the :py:class:`WhoCommand` needs to
    be referenced somewhere, this is the name defined by
    :py:attr:`WhoCommand.__command__` that will be used.

+
    :py:attr:`WhoCommand.__command_name__` is the name of the command. When
    invoking the sub-command, this is the
    :py:attr:`WhoCommand.__command_name__` that will be used.

+
    :py:attr:`WhoCommand.__parent__` is the reference to the parent
    sub-command. If you want that your command to be used as a sub-command of
    another command, simply add the reference name of another command inside
    this variable.

+
    :py:attr:`WhoCommand.__help__` contains the message to display to the user
    when invoking the help.

.. warning::
    In order to the framework be able to discover the command just defined, you
    should be sure of two things:

    + the class of your command inherits the :py:class:`Command` class
    + the class you defined has been imported somewhere before invoking the
      command. Typically an import has been done somewhere, happening before
      the execution of the function in the scripts directory.

Add arguments
-------------

Let's say you are an exigent user and you want to have the possibility to
select the format for displaying the name of the author. A way to do that is
to create arguments that will be used in the command handler. For this, you
just have to define a method to add argument to the created parser for the
command, parser created by :py:mod:`argparse`.

So we add an option to the *who* command to display only the information on
the author name, and a second one to display only the short description.
Rewriting :py:class:`WhoCommand`:

.. code-block:: python

    class WhoCommand(Command):
        """
        Define a command displaying informations on the pipeline author.
        """
        # a name to reference the command
        __command__ = "who_command"

        # the name of the command used
        __command_name__ = "who"

        # the parent of the command
        __parent__ = "master"

        # the help message displayed to the user
        __help__ = "Show the author of the pipeline"

        def add_arguments(self, parser):
            """
            Add arguments to the parser associated to the command.
            """
            # add option for displaying only the author name
            parser.add_argument(
                "--author",
                help="Show only author name",
                action="store_true",
            )

            # same but only for the description
            parser.add_argument(
                "--description",
                help="Show only the description of the author",
                action="store_true",
            )

        def __call__(self, args):
            if args.author:
                print("Super Mario")
            elif args.description:
                print("pipeline expert")
            else:
                print("Super Mario, pipeline expert")

Now doing:

.. code-block:: bash

    $ pipeline who
    Super Mario, pipeline expert

    $ pipeline who --author
    Super Mario

    $ pipeline who --description
    pipeline expert

If you want to specify to the user that the two options are mutually
exclusives, you can the arguments inside a group. Simply modify
:py:meth:`add_aguments` to:

.. code-block:: python

    def add_arguments(self, parser):
        """
        Add arguments to the parser associated to the command.
        """
        # create a group
        group = parser.add_mutually_exclusive_group()

        # add option for displaying only the author name in the group
        group.add_argument(
            "--author",
            help="Show only author name",
            action="store_true",
        )

        # same but only for the description
        group.add_argument(
            "--description",
            help="Show only the description of the author",
            action="store_true",
        )

Now, an error is displayed to the user if both options are given at the same
time.

.. seealso::
    Details on all the large possibilities on how to add arguments and
    actions that can be taken with them are given on :py:mod:`argparse`.

Hierarchy in commands
=====================

Now, you have a working pipeline, with several author contributions, and you
want to show a list of them through the command line interface. This is
clearly related to the *who* command, but you do not want to simply add
arguments, since you may want to add arguments to make some filtration on
the list you want to show. For this, you will need to add a subcommand to
the *who* command. This can be easily achieved by creating a new
:class:`Command`.

.. code-block:: python

    class WhoListCommand(Command):
        """
        Command displaying the list of authors and contributors.
        """
        # reference for the command
        __command__ = "who_list_command"

        # command used in cli
        __command_name__ = "list"

        # here specify that the parent command is the who one
        __parent__ = "who_command"

        # help message to display for this command
        __help__ = "Show a list of contributors to the pipeline"

        def __call__(self, args):
            """
            With the list of authors taken from somewhere, display it when
            invoked as a command.
            """
            # simple message
            print("Contributors:")

            # print the list of authors
            print(", ".join(get_authors_list()))

The command is used like this:

.. code-block:: bash

    $ pipeline who list
    Contributors:
    Super Mario, Luigi, Peach

This is simply the :attr:`__parent__` that will order the hierarchy of
commands between them. You can add any arguments as needed to this command
as described above. The arguments and options will be associated to this
command only.

.. note::

    The framework is very flexible and allows quick development of new
    commands and change in the hierarchy. Let's say that you do not want to
    use the *list* command with the *who* command anymore, and want it to be
    a "root" command, independent of the *who* one. The only thing you have
    to do is changing :attr:`__parent__` to :obj:`__master__` and rename the
    command if you wish with the :attr:`__command_name__` attribute.
    Resulting in a calling interface like this:

    .. code-block:: bash

        $ pipeline list
        Contributors:
        Super Mario, Luigi, Peach

Inheritance of parameters
=========================

Simple use case
---------------

A command that you define can inherit the commands of a parent command if
you specify it. By default, arguments of a command must be placed between
the command and its subcommand, else the arguments will not be recognized.

.. code-block:: bash

    $ pipeline command1 --arg_command1 command2

This command is valid. But the following not:

.. code-block:: bash

    $ pipeline command1 command2 --arg_command1

while it seems natural to do it this way if for example, **--arg_command1**
is equivalent to a **--verbose** option.

To get something similar to the last behaviour, you can use the
:attr:`__parent_arguments__` attribute, containing a list of parent command
names, whose arguments will be inherited.

.. warning::
    If some arguments are conflicting given their names, the latter
    definition of the argument will be used in priority, which should with
    the framework structure, always be the one of the child command.

Advanced use case
-----------------

:mod:`argparse` will always add an *help* option by default to any parser
that it creates. But for inheritance, this behaviour is not always wanted,
since the child command will override the *help* command of the parent
parser. To avoid such situations, you can also create a parser that will not
be used, only as a parent for other parsers. For this, you only need to
override the :meth:`parser` of the :class:`Command`.

.. code-block:: python

    def parser(self, subparser, parents):
        """
        Return the parser for the command and options that this command must
        use. Take as argument the subparser from the parent parser.
        """
        # create a parser with no help
        parser = argparse.ArgumentParser(add_help=False)

        # add the parser to the list of parents
        new_parents = parents + [parser]

        # call the Command class method
        old_parser = super(WhoCommand, self).parser(subparser, new_parents)

        # return this parser
        return parser

This way, a new parser is created with its arguments and no help, given to
the parser of the command as a parent to get its options, and the parser is
returned to be associated to the command, if a child wants to reference its
arguments.

.. warning::
    By using this technique, you are modifying the intrinsic behaviour of
    the framework. While it can be helpful some times, if you don't know
    what you are doing or what you want to do can be achieved in an other
    way, try to avoid this "advanced technique", since side effects will be
    unknown.

API
===

.. automodule:: poppy.core.command
    :members:
    :show-inheritance:

.. autoclass:: poppy.core.command.CommandManager
    :members:
    :inherited-members:

