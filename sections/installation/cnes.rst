*****************
CNES installation
*****************

.. |docker| replace:: **Docker**
.. |psql| replace:: PostgreSQL

This part describes how to build a directory that will be suitable for the
procedure of installation of the ROC softwares inside the machine available at
CNES. A Debian Jessie 8.2 64 bits will be needed to perform all of these steps.
The architecture of the processor must be identical to the one of CNES. At the
time of writing, it is a :code:`Dual Processor Intel Xeon E5-2620 2GHz`.

Detailed installation
=====================

To provide a mastered environment on the CNES machine, without having
directly access to it, the best solution has been to use |docker|
containers, configured with the tested system. See `Docker
<https://docs.docker.com/>`_ for details on how |docker| works.

for details on how to use docker and its principle.

These containers have already been prepared by the ROC team and no
configuration or setup should be necessary on the CNES machine.

Following sections describe steps performed by the installation script.

Docker installation
-------------------

Program installation
^^^^^^^^^^^^^^^^^^^^

Since the CNES machine doesn't have an external access to the world, the
first step is to install the |docker| program on it. Several *.deb*
packages are provided in the *debs* directory of the installation that need
to be installed on the target system. They should be compliant with the
Debian Jessie system.

What the script does is simply installing all of them through *dpkg*:

.. code-block:: bash

    sudo dpkg -i ./debs/*

If there is no problem, then the server used by docker should be started,
through *systemd*:

.. code-block:: bash

    sudo systemctl start docker.service

Images installation
^^^^^^^^^^^^^^^^^^^

Images and containers prepared and created by the ROC team are available in
the *images* directory and must be installed on the docker server to be
usable.

The *rgts-runner* image contains the system, dependencies, libraries and the
ROC software, already configured to be used on the CNES machine.

The *rgts-postgresql* image contains the configuration and installation of
the |psql| database used by the ROC software. The link with previous image
will be done when running the container for the application.

The containers for data are also provided, with one for the data from the
database and the other for the data produced by the CNES team with the ROC
pipeline.

To install them:

.. code-block:: bash

    docker load --input images/runner.tar.gz
    docker load --input images/postgresql.tar.gz
    docker load --input images/busybox.tar.gz
    docker create -v /var/lib/postgresql -v /var/log/postgresql \
        --name dbdata rgts-postgresql /bin/true
    docker run --rm --volumes-from dbdata \
        -v $(pwd)/images/:/backup busybox \
        tar xvfz /backup/pg_log.tar.gz
    docker run --rm --volumes-from dbdata \
        -v $(pwd)/images/:/backup busybox \
        tar xvfz /backup/pg_data.tar.gz
    docker create -v /products --name dataproducts rgts-runner /bin/true

Scripts installation
--------------------

The installation of scripts provided by the ROC team on *scripts* is done in
local, only for the user:

.. code-block:: bash

    mkdir -p ${HOME}/.local/bin
    cp ./scripts/* ${HOME}/.local/bin/

Build installation
==================

Instructions for ROC team on how to build a directory for the installation
on CNES machines.

Download
--------

First download the *Dockerfiles* structure from the SVN repository at:

Install |docker| and packages used to create the *.deb* packages of the
|docker| program:

.. code-block:: bash

    # for packages for deb creation
    sudo apt-get install dpkg-repack debfoster fakeroot

    # for the docker part, since it is not available in the repository
    # if this procedure doesn't work anymore, check how to do in the
    # docker documentation
    sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 \
        --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    sudo echo "deb https://apt.dockerproject.org/repo debian-jessie main" \
        > /etc/apt/sources.list.d/docker.list
    sudo apt-get update
    sudo apt-get install docker-engine
    sudo systemctl start docker

    # add your user to the docker group. Be aware that a user in docker
    # group is equivalent to root
    sudo usermod -a -G docker ${USER}

Generate deb packages
---------------------

Go inside the *debs* directory and do:

.. code-block:: bash

    fakeroot -u dpkg-repack docker-engine
    fakeroot -u dpkg-repack $(debfoster -d docker-engine | sed 1d)


Generating images
-----------------

Go inside the directory where is put the installation procedure and build
the base image.

.. code-block:: bash

    cd procedure
    docker build -t rgts-base -f base/Dockerfile .

The base image will take a moment if not already built. It will download and
install dependencies through *apt-get*, download and install **PyQt5** in a
*virtualenv* that will be used by the ROC software.

Then create the builder image. Start by going in the good directory:

.. code-block:: bash

    cd builder

Put here a *tar.gz* for the pipeline source code and for the TV-SGSE, as
*poppy.tar.gz* for the pipeline and *tvsgse.tar.gz* for the TV-SGSE. You can
do it by retrieving them from the SVN repository and creating a *tar.gz* of
them.

Then you can create the builder image:

.. code-block:: bash

    docker build -t rgts-builder -f builder.docker .

and same for the runner:

.. code-block:: bash

    docker build -t rgts-runner -f runner.docker .

In the same way, for the |psql| image, go inside *postgresql* directory and
build the image as:

.. code-block:: bash

    docker build -t rgts-postgresql -f Dockerfile .

Configuration
-------------

Now configure the pipeline inside images and containers.

First create the container for storing data from the database:

.. code-block:: bash

    docker create -v /var/lib/postgresql -v /var/log/postgresql \
        --name dbdata rgts-postgresql /bin/true

Start a container running the database that use the volumes defined in the
storage container and make the database accessible through the 5433 port
instead of the 5432 if a database is already running on the system:

.. code-block:: bash

    docker run -d -p 5433:5432 --volumes-from dbdata \
        --name rgts-pg-container rgts-postgresql

Now you can run containers to use the pipeline and configure it. You must
use a configuration file that is tuned to use the database of the container.
See the documentation of the pipeline and the configuration file section for
details. Hereafter we assume that the default configuration is well
configured.

.. code-block:: bash

    docker run --rm --link rgts-pg-container:localhost rgts-runner \
        rpl create
    docker run --rm --link rgts-pg-container:localhost rgts-runner \
        roc create
    docker run --rm --link rgts-pg-container:localhost rgts-runner \
        piper descriptor

The IDB to use must also be configured inside the database, so you will need
to mount a volume from the host machine inside the container to give it the
possibility to read the IDB files. Let suppose they are present in the host
system inside /home/user/IDB, you can load them inside the database of the
pipeline with:

.. code-block:: bash

    docker run --rm --link rgts-pg-container:localhost rgts-runner \
        -v /home/user/IDB:/IDB \
        rpl load --idb /IDB/RPW_IDB.xml \
        --mapping /IDB/IDB_SRDB_Mapping_Table_by_MapGen.xml

Exporting images
----------------

Now you can export the created images and the containers for data with:

.. code-block:: bash

    docker save --output runner.tar.gz rgts-runner
    docker save --output postgresql.tar.gz rgts-postgresql
    docker save --output busybox.tar.gz busybox
    docker run --rm --volumes-from dbdata -v $(pwd):/backup busybox \
        tar cvfz /backup/pg_data.tar.gz /var/lib/postgresql
    docker run --rm --volumes-from dbdata -v $(pwd):/backup busybox \
        tar cvfz /backup/pg_log.tar.gz /var/log/postgresql

Archive
-------

Now create an archive of the installation directory you just have created:

.. code-block:: bash

    tar cvfz --transform 's,^\.,ROC_${VERSION}' ${VERSION}.tar.gz .

.. vim: set tw=79 :
