import sys
import os

# add roc_sphinx root to the python path
sys.path.append(os.path.abspath('.'))

# import the common sphinx configuration
from common_conf import *
