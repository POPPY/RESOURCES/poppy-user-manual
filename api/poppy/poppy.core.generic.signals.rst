poppy.core.generic.signals module
=================================

.. automodule:: poppy.core.generic.signals
    :members:
    :undoc-members:
    :show-inheritance:
