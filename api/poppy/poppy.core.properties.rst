poppy.core.properties module
============================

.. automodule:: poppy.core.properties
    :members:
    :undoc-members:
    :show-inheritance:
