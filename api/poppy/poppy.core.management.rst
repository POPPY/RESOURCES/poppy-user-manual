poppy.core.management package
=============================

Submodules
----------

.. toctree::

   poppy.core.management.commands

Module contents
---------------

.. automodule:: poppy.core.management
    :members:
    :undoc-members:
    :show-inheritance:
