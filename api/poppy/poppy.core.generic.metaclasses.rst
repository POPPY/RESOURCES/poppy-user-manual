poppy.core.generic.metaclasses module
=====================================

.. automodule:: poppy.core.generic.metaclasses
    :members:
    :undoc-members:
    :show-inheritance:
