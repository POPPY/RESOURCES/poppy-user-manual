poppy.core.tools.appdirs package
================================

Submodules
----------

.. toctree::

   poppy.core.tools.appdirs.appdirs

Module contents
---------------

.. automodule:: poppy.core.tools.appdirs
    :members:
    :undoc-members:
    :show-inheritance:
