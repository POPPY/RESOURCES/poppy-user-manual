poppy.core.logger module
========================

.. automodule:: poppy.core.logger
    :members:
    :undoc-members:
    :show-inheritance:
