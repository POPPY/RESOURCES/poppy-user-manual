poppy.core.configuration module
===============================

.. automodule:: poppy.core.configuration
    :members:
    :undoc-members:
    :show-inheritance:
