poppy package
=============

Subpackages
-----------

.. toctree::

    poppy.core

Module contents
---------------

.. automodule:: poppy
    :members:
    :undoc-members:
    :show-inheritance:
