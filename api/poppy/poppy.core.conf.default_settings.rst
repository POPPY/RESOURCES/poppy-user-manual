poppy.core.conf.default_settings module
=======================================

.. automodule:: poppy.core.conf.default_settings
    :members:
    :undoc-members:
    :show-inheritance:
