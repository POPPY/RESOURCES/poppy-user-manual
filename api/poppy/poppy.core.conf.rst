poppy.core.conf package
=======================

Submodules
----------

.. toctree::

   poppy.core.conf.default_settings

Module contents
---------------

.. automodule:: poppy.core.conf
    :members:
    :undoc-members:
    :show-inheritance:
