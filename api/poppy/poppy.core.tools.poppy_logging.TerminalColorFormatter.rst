poppy.core.tools.poppy_logging.TerminalColorFormatter module
============================================================

.. automodule:: poppy.core.tools.poppy_logging.TerminalColorFormatter
    :members:
    :undoc-members:
    :show-inheritance:
