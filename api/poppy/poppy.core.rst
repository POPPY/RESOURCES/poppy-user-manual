poppy.core package
==================

Subpackages
-----------

.. toctree::

    poppy.core.conf
    poppy.core.db
    poppy.core.generic
    poppy.core.management
    poppy.core.tests
    poppy.core.tools

Submodules
----------

.. toctree::

   poppy.core.command
   poppy.core.configuration
   poppy.core.logger
   poppy.core.properties
   poppy.core.task
   poppy.core.test

Module contents
---------------

.. automodule:: poppy.core
    :members:
    :undoc-members:
    :show-inheritance:
